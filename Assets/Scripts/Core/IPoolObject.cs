﻿using UnityEngine;

namespace Core {
	public delegate void UnUseDelegate(EnemyType enemyType, IPoolObject poolObject);

	public interface IPoolObject {
		event UnUseDelegate OnUnUse;
		bool IsActive { get; }
		void Activate();
		void Deactivate();
		void UnUse();
		void Init();
		bool Contains(IPoolObject poolObject);
		void SetPosition(Vector3 position);
		void SetRotation(Quaternion quaternion);
	}
}