using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Core {
	public class MonsterFactory : MonoBehaviour {
		[SerializeField] private List<PoolPrefabWrapper> poolPrefabWrappers;

		public T Create<T>(EnemyType enemyType) where T : class, IPoolObject {
			var concretePrefabWrapper = poolPrefabWrappers.FirstOrDefault(wrapper => wrapper.EnemyType == enemyType);
			if (concretePrefabWrapper == null) {
				Debug.LogError("Wrapper does not exists.");
				return default;
			}

			return concretePrefabWrapper.Create() as T;
		}
	}
}