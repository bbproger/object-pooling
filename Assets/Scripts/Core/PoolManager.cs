using System;
using System.Collections.Generic;
using UnityEngine;

namespace Core {
	public enum EnemyType {
		Square,
		Triangle,
		Circle
	}

	public class PoolManager : MonoBehaviour {
		[SerializeField] private MonsterFactory monsterFactory;
		[SerializeField] private int defaultCount;
		private Dictionary<EnemyType, Queue<IPoolObject>> pool;
		private Dictionary<EnemyType, List<IPoolObject>> activePool;

		private void Start() {
			pool = new Dictionary<EnemyType, Queue<IPoolObject>>();
			activePool = new Dictionary<EnemyType, List<IPoolObject>>();
		}

		public void CreateRange<T>(EnemyType enemyType, int count) where T : class, IPoolObject {
			for (int i = 0; i < count; i++) {
				Create<T>(enemyType);
			}
		}

		public void Create<T>(EnemyType enemyType) where T : class, IPoolObject {
			var enemyObject = monsterFactory.Create<T>(enemyType);
			enemyObject.OnUnUse += UnUse;
			enemyObject.Init();
			enemyObject.Deactivate();
			if (!pool.ContainsKey(enemyType)) {
				pool.Add(enemyType, new Queue<IPoolObject>());
			}

			pool[enemyType].Enqueue(enemyObject);
		}

		public T Use<T>(EnemyType enemyType) where T : class, IPoolObject {
			if (!pool.ContainsKey(enemyType)) {
				CreateRange<T>(enemyType, defaultCount);
				return Use<T>(enemyType);
			}

			if (pool[enemyType].Count == 0) {
				Create<T>(enemyType);
				return Use<T>(enemyType);
			}

			var poolObject = pool[enemyType].Dequeue();
			poolObject.Activate();
			if (!activePool.ContainsKey(enemyType)) {
				activePool.Add(enemyType, new List<IPoolObject>());
			}

			activePool[enemyType].Add(poolObject);
			return poolObject as T;
		}

		public void UnUse(EnemyType enemyType, IPoolObject poolObject) {
			int activePoolItemIndex = activePool[enemyType]
				.FindIndex(activePoolObject => activePoolObject.Contains(poolObject));
			if (activePoolItemIndex < 0) {
				Debug.LogError("Can't find enemy");
				return;
			}
			activePool[enemyType].RemoveAt(activePoolItemIndex);
			poolObject.Deactivate();
			pool[enemyType].Enqueue(poolObject);
		}
	}
}