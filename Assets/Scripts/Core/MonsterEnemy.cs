using System;
using UnityEngine;

namespace Core {
	public abstract class MonsterEnemy : Enemy, IPoolObject {
		[SerializeField] private EnemyType monsterType;
		public bool IsActive => gameObject.activeSelf;
		public event UnUseDelegate OnUnUse;

		private string id;

		public void Init() {
			id = Guid.NewGuid().ToString();
		}

		public void UnUse() {
			OnUnUse?.Invoke(monsterType, this);
		}

		public virtual void Activate() {
			gameObject.SetActive(true);
		}

		public virtual void Deactivate() {
			gameObject.SetActive(false);
		}

		public bool Contains(IPoolObject poolObject) {
			if (!(poolObject is MonsterEnemy monsterEnemy)) {
				return false;
			}

			return monsterEnemy.id == id;
		}

		public void SetPosition(Vector3 position) {
			transform.position = position;
		}

		public void SetRotation(Quaternion quaternion) {
			transform.rotation = quaternion;
		}
	}
}