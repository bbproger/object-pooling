using System;
using UnityEngine;
using Object = UnityEngine.Object;

namespace Core {
	[Serializable]
	public class PoolPrefabWrapper {
		[SerializeField] private EnemyType enemyType;
		[SerializeField] private MonsterEnemy monsterPrefab;
		[SerializeField] private Transform container;

		public EnemyType EnemyType => enemyType;

		public MonsterEnemy Create() {
			return Object.Instantiate(monsterPrefab, container);
		}
	}
}