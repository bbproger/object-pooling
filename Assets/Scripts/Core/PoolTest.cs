using UnityEngine;

namespace Core {
	public class PoolTest : MonoBehaviour {
		[SerializeField] private PoolManager poolManager;
		private MonsterEnemy activeMonster;
		private MonsterEnemy activeMonster2;

		private void Update() {
			if (Input.GetKeyDown(KeyCode.A)) {
				activeMonster = poolManager.Use<MonsterEnemy>(EnemyType.Square);
				activeMonster.SetPosition(Random.insideUnitCircle * 4f);
				activeMonster.SetRotation(Quaternion.Euler(Vector3.forward * Random.Range(0, 360f)));
			}

			if (Input.GetKeyDown(KeyCode.S)) {
				activeMonster.UnUse();
			}
			
			if (Input.GetKeyDown(KeyCode.Z)) {
				activeMonster2 = poolManager.Use<MonsterEnemy>(EnemyType.Circle);
				activeMonster2.SetPosition(Random.insideUnitCircle * 4f);
				activeMonster2.SetRotation(Quaternion.Euler(Vector3.forward * Random.Range(0, 360f)));
			}

			if (Input.GetKeyDown(KeyCode.X)) {
				activeMonster2.UnUse();
			}
		}
	}
}